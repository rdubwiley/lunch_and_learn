import os
import shutil

FOLDERS = [
    './messy_receipts',
    './organized_receipts'
]


def delete_folders():
    for folder in FOLDERS:
        try:
            shutil.rmtree(folder, ignore_errors=False, onerror=None)
        except Exception:
            print(f"folder {folder} not found")


def create_folders():
    for folder in FOLDERS:
        os.mkdir(folder)


def main():
    delete_folders()
    create_folders()


if __name__ == "__main__":
    main()