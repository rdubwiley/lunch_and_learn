# Tutorial Python project

# Files
1. 'sort_receipts.py' - main file for the tutorial. You need to finish the unfinished functions.
2. 'reset_receipts.py' - resets the receipts folders to the state before running create_receipts.
3. 'create_receipts.py' - generates random receipts and the folder structure for the organized receipts.

# Motivation
### You have a bunch of receipts you've entered into your computer (see messy_receipts).
### Unfortunately, you have the files stored by receipt number, but you want to sort them by store, month, and day.
### You also want to total up the item amounts by store as well as their total costs.

# Goal
### Finish the sort_receipts script by completing the unfinished functions

# Some tips
### I've written a test suite using the pytest framework. 
### When you want to test your code run the command "pytest" from the command line from the main directory.
### Each function has a docstring giving a description of what the function should do. Generally the most important parts here are understanding the input and output.