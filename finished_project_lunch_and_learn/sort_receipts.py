import os
from math import floor


def get_receipt_filepaths(dir):
    """
    Returns all the filenames in the messy_receipts directory
    """
    return [ f"{dir}/{filename}" for filename in os.listdir(dir) ]


def read_receipt_file(loc):
    """
    Reads the receipt file at a given location and returns a dict to store the data.

    Dict is of the form:
    {
        output_path: STR ({store}/{month}/{day}.txt),
        items: [ {item_type: STR, amount: FLOAT, total_cost: FLOAT} ]
    }

    output_path should be 'STORE_NAME/MONTH/DATE.txt'

    Receipt txt file is of the form
    -----
    store_name
    date ({month}/{day})
    item1,amount,total_cost
    ...
    -----
    """
    with open(loc, 'r') as f:
        data = f.read().splitlines()
    store_name = data[0]
    month = data[1].split("/")[0]
    day = data[1].split("/")[1] 
    items = [ item.split(",") for item in data[2:] ]
    itemsdict = [ 
        {
            'item_type': item[0],
            'amount': int(item[1]),
            'total_cost': float(item[2])

        } for item in items
    ]
    return {
        'output_path': f"{store_name}/{month}/{day}.txt",
        'items': itemsdict
    }


def write_receipt_files(receipt_dicts, output_dir):
    """
    From a receipt dict generates a new text file at the output location.

    the output location should be specified from the dict, and at the following location:
    
    {store}/{month}/{day}.txt

    Output file should look like
    ---
    apples,2,0.80
    ---
    """
    for rd in receipt_dicts:
        lines = [ f"{item['item_type']},{item['amount']},{item['total_cost']}" + "\n" for item in rd['items']]
        with open(f"{output_dir}/{rd['output_path']}", 'w') as f:
            f.writelines(lines)


def write_total_files(receipt_dicts, output_dir):
    """
    For a given list of receipt dicts, sorts them into their stores, and then generates and writes totals. 
    
    Files should be stored in the top level of organized receipts.
    
    Names should be {store}_total.txt

    Each item should have a tally of all of its receipts for example, it should look like this:

    ---
    apples,10,4.00
    ---
    """
    for item in receipt_dicts:
        item['store'] = item['output_path'].split("/")[0]
    stores = set([ item['store'] for item in receipt_dicts ])
    items = [ item['items'] for item in receipt_dicts ]
    item_types = set([ subitems['item_type'] for item in items for subitems in item ])
    storedict = {
            store: { item: {'amount': 0, 'total_cost': 0} for item in item_types } for store in stores
    } 
    for rd in receipt_dicts:
        for item in rd['items']:
            storedict[rd['store']][item['item_type']]['amount'] += item['amount']
            storedict[rd['store']][item['item_type']]['total_cost'] += item['total_cost']
    for store in storedict:
        itemdict = storedict[store]
        lines = [
            f"{item},{itemdict[item]['amount']},{floor(100.0*itemdict[item]['total_cost'])/100.0}" + "\n" for item in itemdict if itemdict[item]['amount'] > 0
        ]
        with open(f"{output_dir}/{store}_total.txt", 'w') as f:
            f.writelines(lines)


def main():
    """
    This is the main script
    """
    filepaths = get_receipt_filepaths('./messy_receipts')
    receipt_dicts = [ read_receipt_file(loc) for loc in filepaths ]
    write_receipt_files(receipt_dicts, './organized_receipts')
    write_total_files(receipt_dicts, './organized_receipts')


if __name__ == "__main__":
    main()