from math import floor
import os

import pytest

from ..create_receipts import (
    STORES,
    MONTHS,
    generate_folders
)
from ..sort_receipts import (
    get_receipt_filepaths,
    read_receipt_file,
    write_receipt_files,
    write_total_files
)


@pytest.fixture
def receipt_dicts():
    return [
        {
            "output_path": "Target/September/1.txt",
            "items": [
                {
                    'item_type': 'apples',
                    'amount': 2,
                    'total_cost': 0.80
                }
            ]
        },
        {
            "output_path": "Target/September/2.txt",
            "items": [
                {
                    'item_type': 'apples',
                    'amount': 1,
                    'total_cost': 0.40
                }
            ]
        },
    ]


def test_get_receipt_filepaths(tmpdir, receipt_dicts):
    temp = tmpdir.mkdir("temp")
    for i in range(10):
        path = temp.join(f"{i}.txt")
        open(path, 'w').close()
    paths = get_receipt_filepaths(temp)
    assert paths is not None
    assert len(paths) == 10


def test_read_receipt_file(tmpdir, receipt_dicts):
    temp = tmpdir.mkdir("temp")
    test_receipt = receipt_dicts[0]
    split = test_receipt['output_path'].split("/")
    path = temp.join("0.txt")
    lines = [
        split[0],
        f"{split[1]}/{split[2][:-4]}"
    ]
    for item in test_receipt['items']:
        lines.append(f"{item['item_type']},{item['amount']},{item['total_cost']}")
    lines = [ line + '\n' for line in lines]
    with open(path, 'w') as f:
        f.writelines(lines)
    receipt_dict = read_receipt_file(path)
    assert type(receipt_dict) == dict
    assert receipt_dict['output_path'] == test_receipt['output_path']
    assert receipt_dict['items'] == test_receipt['items']


def test_write_receipt_files(tmpdir, receipt_dicts):
    temp = tmpdir.mkdir("temp")
    generate_folders(temp)
    write_receipt_files(receipt_dicts, temp)
    for rd in receipt_dicts:
        path = temp.join(rd['output_path'])
        assert path.isfile()
        with open(path, 'r') as f:
            data = f.read().splitlines()
        item_check = []
        for item in data:
            split = item.split(",")
            itemdict = {
                'item_type': split[0],
                'amount': int(split[1]),
                'total_cost': float(split[2])
            }
            item_check.append(itemdict)
        assert item_check == rd['items']


def test_write_total_files(tmpdir, receipt_dicts):
    temp = tmpdir.mkdir("temp")
    generate_folders(temp)
    write_total_files(receipt_dicts, temp)
    path = temp.join("Target_total.txt")
    assert path.isfile()
    data = []
    with open(path, 'r') as f:
        data = [ line for line in f.readlines() ]
    data = [ item.split(",") for item in data ]
    datadict = [
        { 
            'item_type': item[0], 
            'amount': int(item[1]), 
            'total_cost': float(item[2]) 
        } for item in data
    ]
    original_stores = set([ item['store'] for item in receipt_dicts ])
    items = [ item['items'] for item in receipt_dicts ]
    original_item_types = set([ subitems['item_type'] for item in items for subitems in item ])
    storedict = {
            store: { item: {'amount': 0, 'total_cost': 0} for item in original_item_types } for store in original_stores
    } 
    for rd in receipt_dicts:
        for item in rd['items']:
            storedict[rd['store']][item['item_type']]['amount'] += item['amount']
            storedict[rd['store']][item['item_type']]['total_cost'] += item['total_cost']
    outputlist = []
    for store in storedict:
        itemdict = storedict[store]
        listitems = [
            {
                'item_type': item,
                'amount': itemdict[item]['amount'],
                'total_cost': floor(100.0*itemdict[item]['total_cost'])/100.0
            }
            for item in itemdict if itemdict[item]['amount'] > 0
        ]
        outputlist.append(listitems)
    assert datadict == listitems

    
    