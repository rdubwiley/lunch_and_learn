import os
from math import floor


def get_receipt_filepaths(dir):
    """
    Returns all the filenames in the messy_receipts directory
    """


def read_receipt_file(loc):
    """
    Reads the receipt file at a given location and returns a dict to store the data.

    Dict is of the form:
    {
        output_path: STR ({store}/{month}/{day}.txt),
        items: [ {item_type: STR, amount: FLOAT, total_cost: FLOAT} ]
    }

    output_path should be 'STORE_NAME/MONTH/DATE.txt'

    Receipt txt file is of the form
    -----
    store_name
    date ({month}/{day})
    item1,amount,total_cost
    ...
    -----
    """


def write_receipt_files(receipt_dicts, output_dir):
    """
    From a receipt dict generates a new text file at the output location.

    the output location should be specified from the dict, and at the following location:
    
    {store}/{month}/{day}.txt

    Output file should look like
    ---
    apples,2,0.80
    ---
    """


def write_total_files(receipt_dicts, output_dir):
    """
    For a given list of receipt dicts, sorts them into their stores, and then generates and writes totals. 
    
    Files should be stored in the top level of organized receipts.
    
    Names should be {store}_total.txt

    Each item should have a tally of all of its receipts for example, it should look like this:

    ---
    apples,10,4.00
    ---
    """


def main():
    """
    This is the main script
    """
    filepaths = get_receipt_filepaths('./messy_receipts')
    receipt_dicts = [ read_receipt_file(loc) for loc in filepaths ]
    write_receipt_files(receipt_dicts, './organized_receipts')
    write_total_files(receipt_dicts, './organized_receipts')


if __name__ == "__main__":
    main()