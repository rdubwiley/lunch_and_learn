import os
from random import sample, randint

TOTAL_RECEIPTS = 50

MAX_SINGLE_ITEM_COUNT = 5

STORES = {
    'Target',
    'Meijer',
    'Kroger'
}

MONTHS = {
    'September',
    'October',
    'November',
    'December'
}

ITEMS = {
    ('bananas', 0.50),
    ('apples', 0.40),
    ('grapes', 0.65),
    ('oranges', 0.75),
    ('peaches', 0.60)
}


def build_random_receipt():
    item_count = randint(1,len(ITEMS))
    items = sample(ITEMS, 2)
    item_counts = [ randint(1, MAX_SINGLE_ITEM_COUNT) for i in range(item_count)]
    item_tuples = list(zip(items, item_counts))
    month = sample(MONTHS, 1)[0]
    day = randint(1, 28)
    store = sample(STORES, 1)[0]
    return {
        'store': store,
        'month': month,
        'day': day,
        'items': item_tuples
    }


def save_receipt(receipt_dict, output_dir, receipt_num):
    path = f"./{output_dir}/{receipt_num}.txt"
    lines = []
    lines.append(receipt_dict['store'] + '\n')
    lines.append(f"{receipt_dict['month']}/{receipt_dict['day']}" + '\n')
    for item in receipt_dict['items']:
        newline = f"{item[0][0]},{item[1]},{round(item[0][1]*item[1],2)}"
        lines.append(newline+'\n')
    with open(path, 'w') as f:
        f.writelines(lines)


def generate_folders(output_dir):
    for store in STORES:
        os.mkdir(f"{output_dir}/{store}")
        for month in MONTHS:
            os.mkdir(f"{output_dir}/{store}/{month}")


def main():
    date_checker = { month:set() for month in MONTHS}
    for i in range(TOTAL_RECEIPTS):
        while True:
            random_receipt = build_random_receipt()
            if random_receipt['day'] not in date_checker[random_receipt['month']]:
                save_receipt(random_receipt, './messy_receipts',i)
                date_checker[random_receipt['month']].add(random_receipt['day'])
                break
    generate_folders('./organized_receipts')

if __name__ == "__main__":
    main()